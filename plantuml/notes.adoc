= plantuml: sandboxing notes
:author: David Buret
:source-highlighter: highlightjs
:highlightjs-theme: atelier-dune-light
:sectnums:
:toclevels: 4
:imagesdir: images/
:toc-title: Sommaire
:stylesheet: dburet.css
:icons: font
:appendix-caption: Annexe
:toc:
:allow-uri-read: true
:safe-mode-level: 10
ifndef::env-gitlab[]
:plantuml-server-url: http://192.168.1.49:23318
endif::[]

NOTE: I'm not using plantuml to draw UML, just using it as a diagram generator.

== Samples

.got to check why the following is not working...

 plantuml::./style-test-rectangle.puml[format=svg]

=== our flow diags

==== My 2019 style

[plantuml,svg]
----

include::style-test-rectangle-old.puml[]

----

==== clean rectangles in 3D

[plantuml,svg]
----

include::style-test-rectangle.puml[]

----

==== handwritten
[plantuml,svg]
----
include::style-test-handwritten.puml[]
----

=== classic

==== test

[plantuml,svg]
----

include::style-test-3.puml[]

----

==== with shadows

==== test

[plantuml,svg]
----

include::style-test-3-shadow.puml[]

----

==== handwritten 

[plantuml,svg]
----

include::style-test-handwritten-2.puml[]

----

=== Mindmap

[plantuml,svg]
----

include::style-test-mindmap.puml[]

----

== notes on styling 

=== skinparameters

You can display all the possibilities with 

    @startuml
    skinparameters
    @enduml

    @startuml
    help skinparams
    @enduml



[appendix]
== document control

* Last edited: {docdate}
* generated with AsciiDoctor version {asciidoctor-version}, backend {backend}
* source: {docname}
* safe mode: {safe-mode-name}

